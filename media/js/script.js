var windowWidth = $(window).width();
var windowHeight = $(window).height();

/*
*  browser detector function
*/
navigator.sayswho= (function(){
    var ua= navigator.userAgent, tem, 
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\bOPR\/(\d+)/)
        if(tem!= null) return 'Opera '+tem[1];
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();
$(function() {
	$('.eip-mobile-nav').toggle(function(oddClick){
		 if ($('#active').text() === 'FUNDS') {
			$('.eip-dropdown-menu-mobile').show();
		 }
		 
		 $('.nav-mobile').show();
		 $('.page-label').hide();
	  
   }, function(evenClick){
		 if ($('#active').text() === 'FUNDS') {
			$('.eip-dropdown-menu-mobile').hide();
		 }
		 
		 $('.nav-mobile').hide();
		 $('.page-label').show();
   });
});
	   
$(function(){
   
   /*
   * modals (disclaimer and login)
   */
   
   
   if (navigator.sayswho.match(/MSIE 8/g)) {
      
      $('.eip-nav-ul-mobile li, .eip-dropdown-menu-mobile').stop().fadeTo(1,0.95);
      $('.eip-dropdown-menu-mobile').hide();
      
      $('.eip-nav-ul-mobile li').hover(function(hoverIn){
         $(this).stop().fadeTo(1,0.9);
      }, function(hoverOut){
         $(this).stop().fadeTo(1,0.95);
      });
   }
   
   
   if ($('#active').text() === 'FUNDS') {
      $('.eip-dropdown-menu').show();
      $('#mobile-active').parent('li').css({background:'#002549',opacity:'.9',border:'none'});
      $( '.eip-dropdown-menu-mobile').find('a').each(function (index) {
          if (index === 0 ||  index === 2) {
/*             $(this).addClass('eip-border-top');
            $(this).addClass('eip-border-bottom'); */
            $(this).css('cssText','border:1px solid #000 !important;');
            /* this.style.setProperty( 'border-bottom', '1px solid black', 'important' );
            this.style.setProperty( 'border-top', '1px solid black', 'important' ); */
          }
      });
      
      if (navigator.sayswho.match(/Firefox/g) !== null) {
         $('#mobile-active').parent().next().css({'margin-top':204+'px'});

      } else if (navigator.sayswho.match(/IE/g) !== null) {
         $('#mobile-active').parent().next().css({'margin-top':202+'px'});
         $('.eip-dropdown-menu-mobile').css({top:300+'px'});
      } else if (navigator.sayswho === 'Safari 4') {
         $('#mobile-active').parent().next().css({'margin-top':215+'px'});
      } else {
         /* $('.eip-dropdown-menu-mobile').css({top:300+'px'}); */
         $('#mobile-active').parent().next().css({'margin-top':204+'px'});  
      }
   }
   
   /*
   *  login close
   */
  /*  $('#login .eip-close').click(function(){
      eipModal({display:'hide',target:'#login'});
   }); */
  
   $('body').click(function(){
      if ($('#active').text() === 'FUNDS') {
         $('.eip-dropdown-menu-mobile').hide();
      }
         
      $('.nav-mobile').hide();
      $('.page-label').show();
   }); 
   
   /*
   *  Toggle button for nav
   */
   
   
   /*
   *  Used in iPad and iPhone to block its zoom feature when rotating
   */
   if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
      var viewportmeta = document.querySelector('meta[name="viewport"]');
      if (viewportmeta) {
         viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
         document.body.addEventListener('gesturestart', function () {
            viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1.6';
         }, false);
      }
   }   
});