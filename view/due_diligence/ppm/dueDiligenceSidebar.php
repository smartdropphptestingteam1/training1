<!--content-->
<div id="content">
   <!--left sidebar-->
     <h2 id="content-title" class="otherTitle">Due Diligence</h2>
		  <div id="leftSidebar">
			 <div class="border-right">
				<div class="linksLeft">	
					  <ul>
						 <li><a href="/view/due_diligence/aima/dueDiligence.php">AIMA Questionnaire</a></li>
						 <li><a id="active"  href="/view/due_diligence/ppm/dueDiligence.php">PPM</a></li>
						 <li><a href="/view/due_diligence/audited/dueDiligence.php">Audited Accounts</a></li>
					  </ul>
				</div>
           <div class="sidebarImgWrapper">
            <img src="/media/images/due-diligence/ppm.png" class="sidebarimage">
            </div>
          </div>
		  </div>
	   <!--end left sidebar-->