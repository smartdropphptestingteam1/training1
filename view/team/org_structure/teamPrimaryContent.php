   <!--primary content-->
   <div class="primaryContent teamPrimaryContent" id="teamContent">
      <div class="org-struct">
	     <ol class="head">
         <li>Board of Directors</li>
         <li>Tobias Bland <br/> CEO & HEAD of Trading</li>
	     </ol>
        <!-- <div id ="top-org-struct"> -->
			<!--  <div class="head">
				<p>Board of Directors</p>
          </div>
			 <div class="head">
				 <p>Tobias Bland <br/> CEO & HEAD of Trading</p>
			</div> -->
		<!-- </div> -->
         <table border="0" class="body">
            <thead align="center" class="bg-info">
               <td>Alpha</td>
               <td>Beta</td>
               <td>Operations</td>
               <td>Legal & Compliance</td>
               <td>Business Development</td>
            </thead>
            <tr>
               <td class="eip-staff">
                  <p>Nico Nicoletti</p>
                  <p>Chief Investment Officer</p>
               </td>
               <td class="eip-staff">
                  <p>Paul So</p>
                  <p>Head of Beta Products</p>
               </td>
               <td class="eip-staff">
                  <p>David Lau</p>
                  <p>Chief Operating Officer</p>
               </td>
               <td class="eip-staff">
                  <p>Josephine Tang</p>
                  <p>Head of Legal & Comp</p>
               </td>
               <td class="eip-staff">
                  <p>Rui Tand</p>
                  <p>Head of Business Dev.</p>
               </td>
            </tr>
            <tr>
               <td class="eip-staff">
                  <p>Yan Li</p>
                  <p>Quant.Analyst & Dealer</p>
               </td>
               <td class="eip-staff">
                  <p>Alex Hsieh</p>
                  <p>Portfolio Manager</p>
               </td>
               <td class="eip-staff">
                  <p>Winnie Chik</p>
                  <p>Sr. Operations Manager</p>
               </td>
               <td class="org-hidden"></td>
               <td class="eip-staff">
                  <p>Kennis Lee</p>
                  <p>Sales Manager</p>
               </td>
            </tr>
            <tr>
               <td class="org-hidden"></td>
               <td class="eip-staff">
                  <p>Adan Yin</p>
                  <p>Portfolio Manager</p>
               </td>
               <td class="eip-staff">
                  <p>Sylvia Chan</p>
                  <p>Operations & Risk Mgr.</p>
               </td>
               <td class="org-hidden"></td>
               <td class="org-hidden"></td>
            </tr>
            <tr>
               <td class="org-hidden"></td>
               <td class="eip-staff">
                  <p>Angie Yap</p>
                  <p>Dealer</p>
               </td>
               <td class="eip-staff">
                  <p>Emily Wong</p>
                  <p>Operations Analyst</p>
               </td>
               <td class="org-hidden"></td>
               <td class="org-hidden"></td>
            </tr>
            <tr>
               <td class="org-hidden"></td>
               <td class="org-hidden"></td>
               <td class="eip-staff">
                  <p>Theresa Tsoi</p>
                  <p>Officer Manager</p>
               </td>
               <td class="org-hidden"></td>
               <td class="org-hidden"></td>
            </tr>
         </table>
      </div>
   </div>
<!--end primary content-->
</div>
<!--end content-->
