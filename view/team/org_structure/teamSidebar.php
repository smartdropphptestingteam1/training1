<!--content-->
<div id="content">
  <h2 id="content-title" class="otherTitle">Team</h2>
   <!--left sidebar-->
   <div id="leftSidebar" class="eip-team-org-struct">
      <div class="border-right">
         <div class="linksLeft" id="eip-team-org-sidebar">
            <ul>
             <li><a id="active" href="/view/team/org_structure/team.php">Organization Structure</a></li>
             <li><a href="/view/team/team_personel/team.php">Tobias Bland</a></li>
             <li><a href="">Nicola Nicoletti</a></li>
             <li><a href="">Paul So</a></li>
             <li><a href="">Alex Hsieh</a></li>
             <li><a href="">Ada Yin</a></li>
             <li><a href="">David Lau</a></li>
             <li><a href="">Josephine Tang</a></li>
             <li><a href="">Rui Tang</a></li>
             <li><a href="">Kennis Lee</a></li>
           </ul>
         </div>
         <div class="sidebarImgWrapper">
            <img src="/media/images/team/org-structure-logo.png" class="sidebarimage">
         </div>
      </div>
   </div>
   <!--end left sidebar-->