   <!--primary content-->
   <div class="primaryContentRight teamPrimaryContent" id="teamPersonnelContent">
      <h3 id="team">Tobias (Toby) Bland - Chief Executive Officer</h3>
      <p>
         Tobias Bland has the primary responsibility for managing Enhanced Investment Products
      </p>
      <p>
         Prior to founding EIP in December 2001, Toby spent 8 years at Jardine 
         Fleming Securities Limited (now JP Morgan), where he was responsible for 
         establishing and running the proprietary trading desk. As the head of 
         proprietary trading, he was responsible for a relative value portfolio invested 
         in South East Asia. Prior to establishing the proprietary trading desk, Toby 
         was involved in convertible bond, tax and warrant arbitrage. He joined 
         Jardine Fleming in 1993 as a member of the securities lending department in 
         Hong Kong.
      </p>
      <p>
         Toby has a Bachelor of Sciences degree from Southampton University, 
         United Kingdom. He has obtained qualifications in financial derivatives and 
         financial engineering, as a broker's representative and as an options/futures
         trading officer with the Stock Exchange of Hong Kong. He is a patron of 
         HOPE for Children, and dedicates time to philanthropic pursuits in Asia. 
      </p>
   </div>
<!--end primary content-->
</div>
<!--end content-->
