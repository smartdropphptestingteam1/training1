<!--content-->
<div id="content">
   <!--left sidebar-->
     <h2 id="content-title" class="otherTitle">Team</h2>
	   <div id="leftSidebar">
	   	  <div class="border-right">
		     <div class="linksLeft">
               <ul>
                <li><a href="/view/team/org_structure/team.php">Organization Structure</a></li>
                <li><a id="active" href="/view/team/team_personel/team.php">Tobias Bland</a></li>
                <li><a href="">Nicola Nicoletti</a></li>
                <li><a href="">Paul So</a></li>
                <li><a href="">Alex Hsieh</a></li>
                <li><a href="">Ada Yin</a></li>
                <li><a href="">David Lau</a></li>
                <li><a href="">Josephine Tang</a></li>
                <li><a href="">Rui Tang</a></li>
                <li><a href="">Kennis Lee</a></li>
               </ul>
               <div class="sidebarImgWrapper">
                  <img src="/media/images/team/ceo.png" class="sidebarimage personnel">
               </div>   
           </div>
			</div>
     </div>
   <!--end left sidebar-->