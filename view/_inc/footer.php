<!--footer-->
<footer>
   <p>
      &copy; 2014 E.I.P All Rights Reserved
   </p>
   <ul id="footer-menu">
      <li><a href="">Website by Jump</a></li>
	  <li>    |     </li>
      <li><a href="">Disclaimer</a></li>   
	  <li>    |     </li>
      <li id="menu-last-child"><a href="">Privacy</a></li>
   </ul>
</footer>
<!--end footer-->
<!-- Disclaimer Modal -->
<div class="modal fade" id="disclaimer" tabindex="-1" role="dialog" aria-labelledby="disclaimer" aria-hidden="false">
  <div class="modal-dialog eip-dialog">
    <div class="modal-content eip-modal-content">
      <div class="modal-header eip-modal-header">
        <h4 class="modal-title" >DISCLAIMER</h4>
      </div>
      <div class="modal-body eip-modal-body" >
        <p>Please read the following and click "I Accept" before proceeding.</p>
         <p>
            The following information refers to every page on this web site, whether or not such pages are individually
            disclaimed. By entering this web site, you agree that you have read and understood the information on this page.
         </p>
         <p>
            The information contained in this website is provided for reference only and does not constitute a distribution,
            an offer to sell or a solicitation of an offer to buy any securities or buy or sell an interest in any investment 
            product managed or arranged by Enhanced Investment Products (EIP) or any of its affiliates. Investment
            involves significant risks and the value of the Funds may go down as well as up. One should read the terms and 
            conditions in the Private Placing Memorandum and seek professional advice before making any investment
            decision. Past performance figures are not indicative of future performance.
         </p>
         <p>
            To the best of the knowledge, information and belief of EIP, all information contained herein is accurate as at 
            the date of publication. However, EIP does not warrant, guarantee or represent, expressly or by implication, the
            accuracy, validity or completeness of such information.
         </p>
      </div>
      <div class="modal-footer eip-modal-footer">
        <button id="accept-btn" type="button" class="btn btn-default" data-dismiss="modal">I ACCEPT</button>
        <button id="dont-accept-btn" type="button" class="btn btn-default">I DO NOT ACCEPT</button>
      </div>
    </div>
  </div>
</div>
<!-- end Disclaimer Modal -->
<!-- Login Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="disclaimer" aria-hidden="false" style="">
  <div class="modal-dialog eip-dialog">
    <div class="modal-content eip-modal-content">
      <div class="modal-header eip-modal-header">
        <button type="button" class="close eip-modal-close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="clear:right;">LOGIN</h4>
      </div>
      <div class="modal-body eip-modal-body">
         <p>Please login with your username and password:</p>
         <p>Username:</p>
         <input type="text" name="username">
         <p style="">Password:</p>
         <input type="password" name="password">
      </div>
      <div class="modal-footer eip-modal-footer">
        <button id="login-btn" type="button" class="btn btn-default" data-dismiss="modal">LOGIN</button>
      </div>
    </div>
  </div>
</div>
<!-- end Login Modal -->
