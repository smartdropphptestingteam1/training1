<!--header-->
<div class="row eip-row">
   <header class="col-sm-12" style="">
	   <!--site logo-label-->
	   <div class="eip-title" style="">            
		  <a href="/"><img src="/media/images/logo.png">
		  <p style="">
			 Enhanced Investment <br>
			 Products Limited
		  </p></a>
	   </div>
	   <!--end logo-label-->
   <!--navigation-->
      <!--navigation for desktop and tablet-->   
		<nav class="nav-tab-desk">
		   <ul class="eip-nav-ul" style="">
			  <li style=""><a style="" href="/view/about/about.php" >ABOUT</a></li>
			  <li id="eip-hover"  style="">
				 <a id="active" href="/view/funds/funds.php" id="active">FUNDS</a>
			  </li>   
			  <li style="" ><a style="" href="/view/team/org_structure/team.php">TEAM</a></li>
			  <li style="" ><a style="" href="/view/due_diligence/aima/dueDiligence.php">DUE DILIGENCE</a></li>
			  <li style="" ><a style="" href="/view/press/press.php">PRESS</a></li>
			  <li class="eip-last-li"><a style="" href="/view/contact/contact.php">CONTACT</a></li>
		   </ul>
		</nav>
      <div class="eip-dropdown-menu">
         <ul class="">
            <li><a href="/view/funds/enhancedIndexedFunds/funds.php">Enhanced Index Funds</a></li>
            <li><a href="/view/funds/EIPOverlayFund/funds.php">E.I.P Overlay Fund</a></li>
            <li class="eip-last-li"><a href="">XIE Shares</a></li>
         </ul>
      </div>
      <!--end navigation for desktop and tablet-->   
      
      <!--navigation for mobile-->   
      <button class="eip-mobile-nav"> 
         <span class="eip-iconbar"></span>
         <span class="eip-iconbar"></span>
         <span class="eip-iconbar"></span>
		</button>
      <nav class="nav-mobile">
		   <ul class="eip-nav-ul-mobile" style="">
			  <li style=""><a style="" href="/view/about/about.php" >ABOUT</a></li>
			  <li id="eip-dropdown" style="">
				 <a id="mobile-active" href="/view/funds/funds.php">FUNDS</a>
			  </li>   
			  <li style="" ><a style="" href="/view/team/org_structure/team.php">TEAM</a></li>
			  <li style="" ><a style="" href="/view/due_diligence/aima/dueDiligence.php">DUE DILIGENCE</a></li>
			  <li style="" ><a style="" href="/view/press/press.php">PRESS</a></li>
			  <li class="eip-last-li"><a style="" href="/view/contact/contact.php">CONTACT</a></li>
		   </ul>
		</nav>
      <div class="eip-dropdown-menu-mobile">
         <ul class="">
             <li><a href="/view/funds/enhancedIndexedFunds/funds.php">Enhanced Index Funds</a></li>
            <li><a href="/view/funds/EIPOverlayFund/funds.php">E.I.P Overlay Fund</a></li>
            <li class="eip-last-li"><a href="">XIE Shares</a></li>
         </ul>
      </div>
      <div class="page-label">
         <span>FUNDS</span>
      </div>
      <!--end navigation for mobile-->  
	<!--end navigation-->
   </header>
</div>
<!--end header-->