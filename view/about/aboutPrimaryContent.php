<!--content-->
<div id="content">
     <!---<div id="leftSidebar">
     </div>-->
       <div class="primaryContent">
         <h2>Company Profile</h2>
         <p>EIP is a Hong Kong based asset management firm founded in 2002. We
         currently employ a team of 15, with over 50 collective years of trading
         experience from the investment team. Our investors consist of bluechip
         institutions from all over the world. EIP's philosophy is to enhance
         investment opportunities for our investors by providing a suite of Alpha and
         Beta products.</p>
         <p>Our Beta business includes eight country specific Enhanced Index Funds
         designed to track and outperform the MSCI Emerging Asia Index in each
         perspective country. In addition, we also manage a suite of ETFs under the
         brand XIE Shares, which includes seven country specific ETFs tracking the
         local equity index in each of the seven emerging Asian countries. These
         products offer investors low-cost Beta options to access emerging Asia.</p>
         
         <h3>Enhanced Index Funds</h3>
            <p>China, India, Indonesia, Korea, Malaysia, Philippines, Taiwan, Thailand</p>
         <h3>XIE Shares</h3>
            <p>For more information on our ETFs, please visit <a href="www.xieshares.com">www.xieshares.com</a>.</p>
            <p>On the Alpha side, we take a low-risk, capital preservation approach to active
                fund management. Over the past ten years, we have demonstrated our
                strength by continuing to excel and provide absolute return even in unstable
                economic conditions such as SARS and the financial crisis of 2008.</p>
         <h3>E.I.P. Overlay Funds</h3>
            <p>Actively managed, absolute return, downside protection, market neutral, liquid</p>
     </div>
</div>
<!--end content-->