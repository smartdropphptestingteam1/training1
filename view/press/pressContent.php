<div id="content">
  <!--left sidebar-->
  <h2 id="content-title" class="otherTitle">Press</h2>
  <div class="leftsidebar press-leftsidebar" id="leftSidebar">
      <div class="border-right">
         <div class="linksLeft">	
              <ul> 
                 <li><a href="">2014</a></li>
                 <li><a id="active" href="">2013</a></li>
                 <li><a href="">2012</a></li>
                 <li><a href="">2011</a></li>
               </ul>
         </div>
        
         <div class="sidebarImgWrapper">
            <img src="/media/images/press/press.png" class="sidebarimage">
         </div>
      </div> 
   </div>
   <!--end left sidebar-->
    
   <!--primary content-->
   <hr>
   <div class="primaryContentRight pressPrimaryContent" id="dueDiligenceContent">
      <ol>
         <li>21 May 2013 - <a href="">Hong Kong Economic Journal</a></li>
         <li>08 May 2013 - <a href="">Am730</a></li>
         <li>07 May 2013 - <a href="">Apple Daily</a></li>
         <li>07 May 2013 - <a href="">Hongkong Economic Journal</a></li>
         <li>06 May 2013 - <a href="">Hongkong Economic Journal</a></li>
         <li>06 May 2013 - <a href="">Macau Dialy</a></li>
         <li>23 April 2013 - <a href="">Hongkong Economic Journal</a></li>
         <li>13 April 2013 - <a href="">Apple Daily</a></li>
         <li>09 April 2013 - <a href="">Hongkong Economic Journal</a></li>
         <li>07 April 2013 - <a href="">Apple Daily</a></li>
         <li>06 April 2013 - <a href="">Economic Digest</a></li>
         <li>02 April 2013 - <a href="">Hongkong Economic Journal</a></li>
         <li>26 March 2013 - <a href="">Hongkong Economic Journal</a></li>
         <li>26 March 2013(b) - <a href="">Hongkong Economic Journal</a></li>
         <li>12 March 2013 - <a href="">Hongkong Economic Journal</a></li>
         <li>26 March 2013 - <a href="">Hongkong Economic Journal</a></li>
      </ol>
  </div>
     <!--end primary content-->
</div>