<!--content-->
<div id="content">
  <div id="leftsidebar">
   </div>
   <div class="primaryContent">
       <h2>Enhanced Investment Products</h2>
         <p>
            Enhanced Investment Products (EIP) was formed in 2002, by the ex-Jardine
            Fleming (now JP Morgan) proprietary trading team. EIP is a boutique asset
            management firm offering a variety of non-correlated Alpha and cost
            effective Beta products, providing investors unique opportunities in Asia's
            emerging markets.
         </p>
            
         <p>
            Since inception, our Beta products have consistently tracked their
            benchmarks and our Alpha products have provided absolute return through
            multiple market cycles. We have accumulated a global institutional investor
            base, and built a solid reputation of providing:
         </p>
            
         <ul>
            <li>A focus on emerging markets</li>
            <li>A quantitative driven approach</li>
            <li>Key emphasis on capital preservation, risk management, liquidity and transparency </li>
            <li>Diversification between uncorrelated strategies</li>
         </ul>
         
         <h2>XIE Shares</h2>
         <p>
            XIE Shares is EIP's ETF brand, offering a suite of country-specific ETFs
              covering 7 markets in Emerging Asia. These funds are the latest addition to
              EIP's cost-effective Beta product offerings in the form of Exchange Traded
              Funds. XIE Shares products are listed in Hong Kong, low cost, transparent
              and simple to use. For more information, please visit our XIE Shares website
             at <a href="www.xieshares.com">www.xieshares.com</a>.
         </p>
    </div>
</div>
<!--end content-->