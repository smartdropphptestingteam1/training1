<div id="content">
   <h2 id="content-title">Enhanced Index Funds</h2>
     <div id="leftSidebar">
		<div class="border-right">
			<div class="linksLeft">	
				  <ul> 
					  <li><a href="/view/funds/enhancedIndexedFunds/funds.php">Enhanced Index Fund Overview</a></li>
					  <li><a href="" id="active">Enhanced Taiwan Index Fund</a></li>
					  <li><a href="">Enhanced China Index Fund</a></li>
					  <li><a href="">Enhanced India Index Fund</a></li>
					   <li><a href="">Enhanced Indonesia Index Fund</a></li>
					   <li><a href="">Enhanced Korea Index Fund</a></li>
					   <li><a href="">Enhanced Malaysia Index Fund</a></li>
					   <li><a href="">Enhanced Philippines Index Fund</a></li>
					  <li> <a href="">Enhanced Thailand Index Fund</a></li>
					  <li> <a href="/view/funds/enhancedIndexedFunds/keyFeatures/funds.php">Key Features</a></li>
					</ul>
			</div>
				<div class="sidebarImgWrapper">
					<img src="/media/images/taiwanIndex.png" class="sidebarimage">
				</div>
		</div>
	</div>
			<div class="primaryContentRight with-mywell">
				<div class="my-well-content">
				   <span class="regional_index_label">Fund Objective</span><br/>
				   <p><span class="not-strong">The Fund aims to beat the MSCI emerging market country index for Taiwan by 100bps, with an optimal target of
							200bps out-performance after costs. EIP presently have Enhanced Index Funds in Taiwan, China, India. Malaysia,
							Korea, Thailand, Indonesia and the Philippines. </span><br/><br/>
						 <span class="not-strong">The Enhanced Taiwan Index Fund is part of a Protected Cell Company domiciled in Mauritius. The E.I.P Overlay
								Fund (a market neutral arbitrage fund) has an agreement to borrow the securities from the Index Fund in order to
								gain short exposure. In return for utilizing the stock held by the Index Fund, the E.I.P Overlay Fund pays 25% of all
								positive performance to the Taiwan fund. This paydown is semi-annual and there is no cross liability.</span></p>
					<br/>
						<span class="regional_index_label">Subscription Frequency</span>: <span class="not-strong">Weekly</span><br/>
						<span class="regional_index_label">Administration fee</span>:<span class="not-strong"> 0.12%</span><br/>
						<span class="regional_index_label">Redemption Frequency</span>:<span class="not-strong"> Monthly</span><br/>
						<span class="regional_index_label">Minimum Investment Size</span>: <span class="not-strong">US$1,000,000</span><br/>
						<span class="regional_index_label">Management Fee</span>:<span class="not-strong"> 0.75%</span><br/><br/>
						<span class="not-strong">ETIFDEX MP (daily pricing DX1 MP)</span><br/>
					
				</div> 
		</div>
	</div>