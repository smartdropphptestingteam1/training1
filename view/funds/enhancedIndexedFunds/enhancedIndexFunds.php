<!--/*this will display the links under the enhanced index funds sub-menu*/ -->
 <div id="content">
	 <h2 id="content-title">Enhanced Index Funds</h2>
     <div id="leftSidebar">
		<div class="border-right">
			<div class="linksLeft">	
				  <ul> 
					  <li><a href="" id="active">Enhanced Index Fund Overview</a></li>
					  <li><a href="/view/funds/enhancedIndexedFunds/taiwanIndex/funds.php">Enhanced Taiwan Index Fund</a></li>
					  <li><a href="">Enhanced China Index Fund</a></li>
					  <li><a href="">Enhanced India Index Fund</a></li>
					   <li><a href="">Enhanced Indonesia Index Fund</a></li>
					   <li><a href="">Enhanced Korea Index Fund</a></li>
					   <li><a href="">Enhanced Malaysia Index Fund</a></li>
					   <li><a href="">Enhanced Philippines Index Fund</a></li>
					  <li> <a href="">Enhanced Thailand Index Fund</a></li>
					  <li> <a href="/view/funds/enhancedIndexedFunds/keyFeatures/funds.php">Key Features</a></li>
					</ul>
			</div>
				
				<div class="my-well">
						<p>
							Fund Objective:<br/>
							 <span class="not-strong">To out-perform the MSCI Price Index 	 annually</span>
						 </p>
						<p>
							Subscription Frequency:<span class="not-strong">Weekly</span>
						</p>
						<p>
							Redemption Frequency:<span class="not-strong">Monthly</span>
						</p>
						<p>
							Management Fee: <span class="not-strong">0.75%</span>
						</p>
						<p>
							Administration Fee: <span class="not-strong">0.12%</span>
						</p>
						<p>
							Minimum Investment Size: <span class="not-strong">US$1,000,000</span>
						</p>
						<p>
							Manager Contacts:<br/>
							Enhanced Investment Products<br/>
							<span class="not-strong">info@eip.com.hk</span>
						</p>
						<p>
							Tel: <span class="not-strong">+852 2110 8600</span>
						</p>
						<p>
							Manager Address<br/>
							<span class="not-strong">337 New Henry House,<br/>
							10 Ice House Street<br/>
							Central, Hong Kong</span>
						</p>
						<p>
							Administrator : <span class="not-strong">Citco</span>
						</p>
						<p>
							Custodian :<span class="not-strong">Deutsche Bank AG London</span>
						</p>
				</div>
			  </div>
		   </div>

		<div class="primaryContentRight">
		  <img src="/media/images/map.png" usemap="#Map" id="map">
			   <p>Providing investment opportunities and 100% exposure to the MSCI benchmark, the Enhanced Index Funds operate in:</p><br/>
				 <ul> <li>Taiwan</li>
				  <li>China</li>
				  <li>India</li>
				  <li>Indonesia</li>
				  <li>Korea</li>
				  <li>Malaysia</li>
				  <li>Philippines</li>
				  <li>Thailand</li>
			   </ul> <br/>
					 <map name="Map">
						<area shape="poly" coords="108,158,110,161,112,163,113,170,114,177,119,182,125,186,130,187,142,188,152,188,161,184,170,175,176,170,170,
						166,162,169,153,170,126,167" href="funds-details.php?id=6">
						
						<area shape="poly" coords="109,122,111,129,111,136,108,140,109,143,109,150,109,158,112,154,116,146,116,141,120,146,122,148,126,156,130,
						157,133,150,137,148,141,147,141,138,137,131,132,123,132,119,129,117,127,113,119,113,115,118" href="funds-details.php?id=8">
						
						<area shape="poly" coords="98,172,106,183,113,192,121,207,130,215,145,219,163,222,176,226,188,230,199,224,205,220,227,221,249,223,256,221,
						261,218,264,223,270,228,280,230,274,220,272,215,281,213,285,208,285,204,279,208,275,210,265,209,258,204,247,199,238,193,226,191,209,188,208,182,
						206,178,203,183,198,184,190,186,182,186,176,184,171,181,176,171,166,179,162,184,150,189,142,188,127,189,111,176,110,171,103,170" href="funds-details.php?id=4">
						
						<area shape="poly" coords="180,126,178,134,178,140,175,152,170,160,178,154,185,155,187,161,185,165,186,168,190,164,191,168,197,171,200,166,200,
						159,197,147,191,140,186,135,188,129,186,123" href="funds-details.php?id=7">
						
						<area shape="poly" coords="38,162,31,144,28,115,22,115,15,107,26,105,23,94,30,91,44,77,42,69,46,68,50,67,63,84,58,87,68,95,80,97,82,99,81,107,82,
						112,73,119,55,131,50,147,53,159,54,167,50,171" href="funds-details.php?id=3">
						
						<area shape="poly" coords="180,103,178,108,180,116,189,116,214,115,214,105,195,104,192,99,185,99" href="funds-details.php?id=1">
						
						<area shape="poly" coords="201,52,200,57,203,57,205,60,203,68,210,66,215,66,209,49" href="fundsdetails.php?id=5">
						
						<area shape="poly" coords="51,67,63,83,77,92,95,91,105,88,110,92,112,96,111,100,111,104,117,112,127,112,135,109,140,115,148,116,145,122,148,125,
						151,122,152,115,162,112,174,107,181,99,188,87,188,80,183,67,193,60,188,58,181,60,178,55,191,48,190,53,196,51,201,52,209,48,220,39,222,30,226,32,232,
						21,225,21,220,16,212,13,210,8,206,2,196,1,193,8,189,12,181,14,177,21,186,22,188,24,182,26,173,31,167,31,163,36,157,40,147,41,140,43,134,38,118,38,113,
						31,105,29,106,22,101,20,97,17,89,21,83,21,80,29,72,32,72,38,68,42,62,44,58,47,51,47,45,51,47,56,48,60" href="funds-details.php?id=2">
						
						<area shape="poly" coords="92,92" href="#">
						</map>
			   <p>The vision of EIP is to provide a one-stop solution to Index investment in Asia's emerging markets.
					  Investors will be able to attain 100% of their MSCI Emerging Asia exposure by investing in the
					  Enhanced Index Funds. By offering 8 country funds, EIP hopes to provide investors the ability to easily
					  switch their allocation between markets in a cost effective manner. Furthermore, investors also have the
					  opportunity to obtain Index out-performance in these markets via the use of market efficient
					  instruments and the arrangement with the E.I.P. Overlay Fund.</p><br/>
			   <p>The Index Funds and the Overlay Fund share a unique relationship, where the Overlay Fund has the
					  right to borrow securities from the Index Funds. In exchange, the Index Funds receive a share of
					  Overlay's positive performance, paid semi-annually. While both set of investors enjoy enhanced
					  performance from this arrangement, there is no cross-liability between the funds as they utilize a
					  Mauritius Protected Cell Company structure. Some investors have chosen to invest in both the Index
					  Funds and the Overlay Fund to achieve a "portable alpha" return profile.</p>
			</div> 
		</div>
		