 <div id="content">
 	<h2 id="content-title">Enhanced Index Funds</h2>
     <div id="leftSidebar">
		<div class="border-right">
			<div class="linksLeft">	
				  <ul> 
					  <li><a href="/view/funds/enhancedIndexedFunds/funds.php">Enhanced Index Fund Overview</a></li>
					  <li><a href="/view/funds/enhancedIndexedFunds/taiwanIndex/funds.php">Enhanced Taiwan Index Fund</a></li>
					  <li><a href="">Enhanced China Index Fund</a></li>
					  <li><a href="">Enhanced India Index Fund</a></li>
					   <li><a href="">Enhanced Indonesia Index Fund</a></li>
					   <li><a href="">Enhanced Korea Index Fund</a></li>
					   <li><a href="">Enhanced Malaysia Index Fund</a></li>
					   <li><a href="">Enhanced Philippines Index Fund</a></li>
					  <li> <a href="">Enhanced Thailand Index Fund</a></li>
					  <li> <a href="" id="active">Key Features</a></li>
					</ul>
			</div>
			<div class="sidebarImgWrapper">
				<img src="/media/images/keyFeatures.png" class="sidebarimage">
			</div>
			</div>
	</div>
		<div class="primaryContentRight">
		      <table class="keyFeatures">
					 <tr><td colspan="2" id="heading">MP Bloomberg Ticker</td></tr>
					 
					 <tr><td class="leftColumn"><span class="not-strong">Taiwan Index Fund </span></td><td><span class="not-strong">ETIFDEX MP Equity</span></td></tr>
					 <tr><td class="leftColumn"><span class="not-strong">India Index Fund</span></td><td><span class="not-strong">EIIFDEX MP Equity </span></td></tr>
					 <tr><td class="leftColumn"><span class="not-strong">China Index Fund</span></td><td><span class="not-strong">ECIFDEX MP Equity</span></td></tr>
					 
					 <tr><td class="leftColumn"><span class="not-strong">Malaysia Index Fund </span></td><td><span class="not-strong">EMIFDEX MP Equity</span></td></tr>
					 <tr><td class="leftColumn"><span class="not-strong">South Korea Index Fund</span></td><td><span class="not-strong">EKIFDEX MP Equity </span></td></tr>
					 <tr><td class="leftColumn"><span class="not-strong">Thailand Index Fund</span></td><td><span class="not-strong">ETHFDEX MP Equity</span></td></tr>
					
					 <tr><td class="leftColumn"><span class="not-strong">Indonesia Index Fund </span></td><td><span class="not-strong">EINFDEX MP Equity</span></td></tr>
					 <tr><td class="leftColumn"><span class="not-strong">Philippines Index Fund</span></td><td><span class="not-strong">EPIFDEX MP Equity </span></td></tr>
					 <tr><td class="leftColumn"><span class="not-strong">Emerging Asia Index Fund </span></td><td><span class="not-strong">EEAFDEX MP Equity</span></td></tr>
					
			  </table>
			  <br/><br/>
			  <table class="keyFeatures">
			     <tr><td id="heading" colspan="2">ISN (Mauritius)</td></tr>
				 
				 <tr><td class="leftColumn"><span class="not-strong">Taiwan Index Fund</span></td><td><span class="not-strong">ETIFDEX MP Equity</span></td></tr>
				 <tr><td class="leftColumn"><span class="not-strong">India Index Fund</span></td><td><span class="not-strong">EIIFDEX MP Equity </span></td></tr>
				 <tr><td class="leftColumn"><span class="not-strong">China Index Fund</span></td><td><span class="not-strong">ECIFDEX MP Equity</span></td></tr>
				 
				 <tr><td class="leftColumn"><span class="not-strong">Malaysia Index Fund</span></td><td><span class="not-strong">EMIFDEX MP Equity</span></td></tr>
				 <tr><td class="leftColumn"><span class="not-strong">South Korea Index Fund</span></td><td><span class="not-strong">EKIFDEX MP Equity</span></td></tr>
				 <tr><td class="leftColumn"><span class="not-strong">Thailand Index Fund</span></td><td><span class="not-strong">ETHFDEX MP Equity</span></td></tr>
				 
				 <tr><td class="leftColumn"><span class="not-strong">Indonesia Index Fund</span></td><td><span class="not-strong">EINFDEX MP Equity</span></td></tr>
				 <tr><td class="leftColumn"><span class="not-strong">Philippines Index Fund</span></td><td><span class="not-strong">EPIFDEX MP Equity</span></td></tr>
				 <tr><td class="leftColumn"><span class="not-strong">Emerging Asia Index Fund</span></td><td><span class="not-strong">EEAFDEX MP Equity</span></td></tr>
			  </table>
			  <br/><br/>
			  <table class="keyFeatures">
					 <tr>
						<td id="heading" class="leftColumn">Paydown Option</td>
						<td><span class="not-strong">25% of gains in E.I.P. Overlay Fund, paid semi-annually</span></td>
					</tr>
					 <tr>
						<td id="heading" class="leftColumn">Management Fee</td>
						<td><span class="not-strong">0.75%</span></td>
					</tr>
				</table>	 
		</div>
	</div>