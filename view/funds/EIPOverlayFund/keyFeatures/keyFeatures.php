   <div id="content">
     <h2 id="content-title">E.I.P Overlay Fund</h2>
      <div id="leftSidebar">
		<div class="border-right">
			<div class="linksLeft">	
				  <ul> 
					  <li><a href="/view/funds/EIPOverlayFund/funds.php" >E.I.P. Overlay Fund</a></li>
					  <li><a href="" id="active">Key Features</a></li>
					  <li><a href="/view/funds/EIPOverlayFund/riskManagement/funds.php">Risk Management</a></li>
					</ul>
			</div>
				<div class="my-well">
						<p>
							Fund Objective:<br/>
							 <span class="not-strong">A non-directional Asia-Ex Japan Investment Structure aimed to produce consistent
								return with low volatility.<br/></span>
						 </p>
						<p>
							Subscription Frequency:<span class="not-strong">Monthly</span>
						</p>
						<p>
							Performance Fee:<span class="not-strong">20%</span>
						</p>
						<p>
							Redemption Frequency: <span class="not-strong">Monthly</span>
						</p>
						<p>
							Minimum Investment Size: <span class="not-strong">US$150,000</span>
						</p>
						<p>
						  Management Fee:<span class="not-strong">2%</span>
						</p>
						<p>
							Investment Advisor:<span class="not-strong">Enhanced Investment Products Ltd 
							(regulated by Hong Kong's SFC)</span>
						</p>
						<p>
							Manager Contacts:<br/>
							<span class="not-strong">Enhanced Investment Products<br/>
							info@eip.com.hk<br/>
							Tel: +852 2110 8600</span>
						</p>
						<p>
							Manager Address<br/>
							<span class="not-strong">337 New Henry House,<br/>
							10 Ice House Street<br/>
							Central, Hong Kong</span>
						</p>
						<p>
							Administrator :<span class="not-strong">Citco</span>
						</p>
						<p>
							Custodian :<span class="not-strong">Deutsche Bank AG London</span>
						</p>
						<p><span class="not-strong">EIPOVER MP Equity (monthly pricing) ISIN code: MU0191S00017</span></p>
				</div>
				<div class="sidebarImgWrapper">
					<img src="/media/images/eipOverlayKeyFeatures.png" class="sidebarimage">
				</div>
			</div>
		</div>
		<div class="primaryContentRight">
				<table class="keyFeatures">
						<tr><td id="heading" class="leftColumn" colspan="2">MP Bloomberg Ticker</td></tr>
						<tr><td class="leftColumn">E.I.P. Overlay Fund</td><td>EIPOVER MP Equity</td></tr>
				</table>
				<br/>
				
				<table class="keyFeatures">
						<tr><td id="heading" class="leftColumn" colspan="2">ISIN (Mauritius)</td></tr>
						<tr><td class="leftColumn">E.I.P. Overlay Fund</td><td>MU091S00017</td></tr>
				</table>
				<br/>
				<table class="keyFeatures">
						<tr><td id="heading" class="leftColumn" colspan="2">KY Bloomberg Ticker</td></tr>
						<tr><td class="leftColumn">E.I.P. Overlay Fund</td><td>KYG3063Y1483</td></tr>
				</table>
				<br/>
				
								
				<table class="keyFeatures">
						<tr><td id="heading" class="leftColumn" colspan="2">ISIN (Cayman)</td></tr>
						<tr><td class="leftColumn">E.I.P. Overlay Fund</td><td>KYG3063Y1483</td></tr>
				</table>
				<br/>
				
				<table class="keyFeatures">
						<tr><td id="heading" class="leftColumn" colspan="2">CUSIP</td></tr>
						<tr><td class="leftColumn">E.I.P. Overlay Fund</td><td>G3063Y148</td></tr>
				</table>
				<br/>
				<table class="keyFeatures">
						<tr><td id="heading" class="leftColumn" colspan="2">Bloomberg Ticker Investment Style</td></tr>
						<tr><td class="leftColumn">EEIPOVER MP Long/short market neutral</td></tr>
				</table>
				<br/>
				
				<table class="keyFeatures">
						<tr><td id="heading" class="leftColumn">Paydown Option </td><td>25% of gain before performance fee, paid semi-annually to Index Funds</td></tr>
						<tr><td id="heading" class="leftColumn">Management Fee</td><td>0.75%</td></tr>
						<tr><td id="heading" class="leftColumn">Performance Fee</td><td>20%</td></tr>
						
						<tr><td id="heading" class="leftColumn">High Water Mark</td><td>Yes</td></tr>
						<tr><td id="heading" class="leftColumn">Hurdle Rate</td><td>5%</td></tr>
						<tr><td id="heading" class="leftColumn">Subscription Frequency </td><td>Monthly</td></tr>
						
						<tr><td id="heading" class="leftColumn">Redemption Frequency</td><td>Monthly</td></tr>
						<tr><td id="heading" class="leftColumn">Redemption Notice</td><td>30 days</td></tr>
						<tr><td id="heading" class="leftColumn">Minimum Investment Size </td><td>US$150,000</td></tr>
						
						<tr><td id="heading" class="leftColumn">Manager</td><td>Enhanced Investment Products (Cayman) Ltd</td></tr>
						<tr><td id="heading" class="leftColumn">Domicile</td><td>Mauritius</td></tr>
						<tr><td id="heading" class="leftColumn">Investment Advisor</td><td>Enhanced Investment Products Ltd (regulated by Hong Kong's SFC)</td></tr>
				</table>
				
				<br/>
				<table class="keyFeatures">
						<tr><td id="heading" class="leftColumn" colspan="2">Advisor Contact</td></tr>
					
						<tr><td  class="leftColumn">Telephone</td><td>+852 2110 8600</td></tr>
						<tr><td  class="leftColumn">Fax</td><td>+852 2119 8089</td></tr>
						<tr><td  class="leftColumn">Email</td><td>info@eip.com.hk</td></tr>
			
				</table>
				<br/>
				
				<table class="keyFeatures">
						<tr><td id="heading" class="leftColumn">Address</td><td>337 New Henry House, 10 Ice House Street Central, Hong Kong</td></tr>
				</table>
				
				<br/>
				<table class="keyFeatures">
						<tr><td id="heading" class="leftColumn">Administrator</td><td>Citco</td></tr>
				</table>
				
				<br/>
				<table class="keyFeatures">
						<tr><td id="heading" class="leftColumn">Administrator</td><td>Deutsche Bank AG London</td></tr>
				</table>
		</div> 
	</div>
		