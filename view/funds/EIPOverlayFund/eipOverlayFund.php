   <div id="content">
    <h2 id="content-title">E.I.P Overlay Fund</h2>
     <div id="leftSidebar">
		<div class="border-right">
			<div class="linksLeft">	
				  <ul> 
					  <li><a href="" id="active">E.I.P. Overlay Fund</a></li>
					  <li><a href="/view/funds/EIPOverlayFund/keyFeatures/funds.php">Key Features</a></li>
					  <li><a href="/view/funds/EIPOverlayFund/riskManagement/funds.php">Risk Management</a></li>
					</ul>
			</div>
				<div class="my-well">
						<p>
							Fund Objective:<br/>
							 <span class="not-strong">A non-directional Asia-Ex Japan Investment Structure aimed to produce consistent
								return with low volatility.</span>
						 </p>
						<p>
							Subscription Frequency:<span class="not-strong">Monthly</span>
						</p>
						<p>
							Performance Fee:<span class="not-strong">20%</span>
						</p>
						<p>
							Redemption Frequency:<span class="not-strong">Monthly</span>
						</p>
						<p>
							Minimum Investment Size:<span class="not-strong">US$150,000</span>
						</p>
						<p>
							Investment Advisor:<span class="not-strong">Enhanced Investment Products Ltd 
							(regulated by Hong Kong's SFC)</span>
						</p>
						<p>
							Manager Contacts:<br/>
							Enhanced Investment Products<br/>
							<span class="not-strong">info@eip.com.hk<br/>
							Tel: +852 2110 8600</span>
						</p><br/>
						<p>
							Manager Address<br/>
							<span class="not-strong">337 New Henry House,<br/>
							10 Ice House Street<br/>
							Central, Hong Kong</span>
						</p><br/>
						<p>
							Administrator :<span class="not-strong">Citco</span>
						</p>
						<p>
							Prime Broker :<span class="not-strong">Deutsche Bank AG London</span>
						</p>
						<p>
							<span class="not-strong">EIPOVER MP Equity (monthly pricing) ISIN code: MU0191S00017</span>
						</p>
				</div>
				<div class="sidebarImgWrapper">
					<img src="/media/images/EIPOverlay.png" class="sidebarimage">
				</div>
			</div>
		</div>
		<div class="primaryContentRight">
			   <p>The E.I.P. Overlay Fund is an Asia ex-Japan market neutral fund aimed to
						consistently produce absolute return without large directional exposure.
						The Fund targets a low volatility, low risk profile by employing a variety of
						equity and derivative strategies.</p><br/>
						<p>During the global economic crisis of 2008, many funds were impacted
						negatively. However, the Overlay Fund continued to thrive and
						demonstrated that its market neutral strategy indeed offers absolute
						return. For the calendar year of 2008, the Fund returned +17.31% (net of
						fees) while the MSCI Emerging Asia Index returned -54.09%. The Fund
						was eventually awarded the Best Market Neutral Fund in the HFMWeek
						Asia Performance Awards 2012.</p><br/>
				 <h3 class="eipOverContent">Unique Advantage</h3>
			   <p>The Overlay Fund has an agreement with the Enhanced Index Funds
						PCC (Index Funds in Taiwan, China, India, Malaysia, Korea, Thailand,
						Indonesia and the Philippines) to utilize the latter's securities in order to
						take advantage of anomalies in the market. In exchange, the Overlay Fund
						pays 25% of its positive performance to the Index Funds. This structure
						gives the Fund a unique advantage in trading the inefficiencies in the
						Asian stock markets with little stock recall risk.</p><br/>
				 <h3 class="eipOverContent">Trading Strategy</h3>
			   <p>The Fund is truly market neutral as it has strict risk limits. The manager
					does not stock pick on a qualitative basis. Trades are primarily generated
					by capturing arbitrage opportunities. The Fund typically participate in
					the following strategies:</p>
				<ul>
				   <li>Delta One Index Arbitrage</li>
				   <li>Stock Borrow and On-Lending</li>
				   <li>Share Class Arbitrage</li>
				   <li>Pairs Trading</li>
				   <li>Event-Driven</li>
				   <li>Convertible Bond Arbitrage</li>
				</ul>
				
			   <img src="/media/images/HFMAwards.png"  class="primaryContentImage">
			</div> 
		</div>
		