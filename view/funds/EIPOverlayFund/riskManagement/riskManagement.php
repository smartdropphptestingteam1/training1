  <div id="content">
    <h2 id="content-title">E.I.P Overlay Fund</h2>
     <div id="leftSidebar">
		<div class="border-right">
			<div class="linksLeft">	
				  <ul> 
					  <li><a href="/view/funds/EIPOverlayFund/funds.php" >E.I.P. Overlay Fund</a></li>
					  <li><a href="/view/funds/EIPOverlayFund/keyFeatures/funds.php" >Key Features</a></li>
					  <li><a href="" id="active">Risk Management</a></li>
					</ul>
			</div>
				<div class="my-well">
						<p>
							Fund Objective:<br/>
							 <span class="not-strong">A non-directional Asia-Ex Japan Investment Structure aimed to produce consistent
								return with low volatility.<br/></span>
						 </p>
						<p>
							Subscription Frequency:<span class="not-strong">Monthly</span>
						</p>
						<p>
							Performance Fee:<span class="not-strong">20%</span>
						</p>
						<p>
							Redemption Frequency:<span class="not-strong">Monthly</span>
						</p>
						<p>
							Minimum Investment Size:<span class="not-strong">US$150,000</span>
						</p>
						<p>
							Management Fee:<span class="not-strong">2%</span>
						</p>
						<p>
							Investment Advisor:<span class="not-strong">Enhanced Investment Products Ltd 
							(regulated by Hong Kong's SFC)</span>
						</p>
						<p>
							Manager Contacts:<br/>
							<span class="not-strong">Enhanced Investment Products<br/>
							info@eip.com.hk<br/>
							Tel: +852 2110 8600</span>
						</p>
						<p>
							Manager Address<br/>
							<span class="not-strong">337 New Henry House,<br/>
							10 Ice House Street<br/>
							Central, Hong Kong</span>
						</p>
						<p>
							Administrator : <span class="not-strong">Citco</span>
						</p>
						<p>
							Prime Broker:<span class="not-strong">Deutsche Bank AG London</span>
						</p>
						<p><span class="not-strong">EIPOVER MP Equity (monthly pricing) ISIN code: MU0191S00017</span></p>
				</div>
				<div class="sidebarImgWrapper">
					<img src="/media/images/EIPOverlayRiskManagement.png" class="sidebarimage">
				</div>
			</div>
		</div>
		<div class="primaryContentRight">
		        <p><span class="not-strong">Comprehensive Framework: Market (Delta), Gamma, Vega, Currency (FX), Interest Rate, Credit,
						Counterparty, and Liquidity risks are all monitored.</span></p>
				<br/><br/>
				<h3>Fund Portfolio and Position Risk Limits</h3><br/><br/>
				<table class="keyFeatures">
					<tr><td id="heading" class="leftColumn">Net Delta-Adj. Exposure (Beta-Adj)</td><td>Max ±20% of NAV (at fund level) <br/>Max ±20% of NAV (for each country)</td></tr>
					<tr><td id="heading" class="leftColumn">Gross Exposure (Delta-Adj)</td><td>Max 500% of NAV</td></tr>
					<tr><td id="heading" class="leftColumn">Currency Exposure</td><td>Max ±20% of NAV net exposure for each currency</td></tr>
					<tr><td id="heading" class="leftColumn">Vega (Net)</td><td>Max ±0.20% of NAV</td></tr>
					
					<tr><td id="heading" class="leftColumn">Vega (Gross)</td><td>Max 2.00% of NAV</td></tr>
					<tr><td id="heading" class="leftColumn">Negative Gamma</td><td>Max -1.00% of NAV</td></tr>
					<tr><td id="heading" class="leftColumn">Theta</td><td>Max -0.60% of NAV (30 days decay)</td></tr>
					
					<tr><td id="heading" class="leftColumn">VaR (95% confidence, 10 days)</td><td>Max 8% of NAV - as per EIP Risk Report and DB Risk Office System (fully independent third party)</td></tr>
					<tr><td id="heading" class="leftColumn">Concentration</td><td>Max 20% of NAV per trade - (excluding index arbitrage)</td></tr>
					<tr><td id="heading" class="leftColumn">Liquidity</td><td>Max 100% of 1-day avg volumes (20 days) <br/>(excl. targets of M&A)</td></tr>
					
					<tr><td id="heading" class="leftColumn">Futures</td><td>Max 5% stake in any company <br/>Max 20% of 1-day avg volumes (10 days)<br/>Max 10% of Open Interest</td></tr>
					<tr><td id="heading" class="leftColumn">Margin Available on PB Account</td><td>More than Expected Shortfall <br/>(expected loss in case the VaR loss level is breached)</td></tr>
					<tr><td id="heading" class="leftColumn">Stop Loss</td><td>1.50% of NAV per month (per trade)</td></tr>
					<tr><td id="heading" class="leftColumn">Limits on Counterparty Exposure</td><td>Rating A or better <br/>CDS 1yr and 5yr less than 500 b.p.</td></tr>
				</table>

		</div> 
	</div>
		