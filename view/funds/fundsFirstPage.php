<div id="content">
			<!--<div id="leftSidebar">
			 </div>--->
			 <div class="border-right"></div>
				 <div class="primaryContent">
						 <h2>Funds</h2>
						 <p>EIP currently offers a suite of funds, and continues to grow with individual mandates and managed accounts. The product offerings can be grouped into two categories: Beta and Alpha. Our Beta products include a series of 8 passive long-only country-specific funds. Each is designed to track and out-perform its respective benchmark. Our Alpha products include 2 multi-strategy hedge funds, which are designed to provide absolute return regardless of the market environment.</p>
							   
						  
							<h3>Beta Products: </h3>
							<h3 class="subHead">Enhanced Index Funds</h3>
								  <p>The Enhanced Index Fund offers immediate access to Asian emerging markets with an option 
								  to outperform their benchmark.</p>
							<h3>Alpha Products: </h3>
							<h3 class="subHead">E.I.P. Overlay Fund</h3>
								  <p>The E.I.P. Overlay Fund is an Asia (ex Japan) multi-strategy market neutral arbitrage fund.</p>	  

							<h3 class="subHead">E.I.P. Aleph Fund</h3>
								  <p>The E.I.P. Aleph Fund is an Asia (incl Japan) multi-strategy fund concentrating on volatility and 
								   relative value strategies.</p>	  
				</div>
			</div>

