<!--content-->
<div id="content" class="contact_mobile">
   <!--left sidebar-->
    <h2 id="content-title" class="otherTitle">Contact</h2>
	   <div id="leftSidebar contactSidebar">
	      <div class="border-right" style="border:0px">
	        <div class="linksLeft contactPage">	
			  <p>
				 For more information please contact:
			  </p>
			  <p>
				 Enhanced Investment Products Limited <br>
					 337 New Henry House,
					 10 Ice House Street, 
					 Central, Hong Kong 
			  </p>
			  <p>
				 <strong class="contact">Tel</strong>: 852 2110 8600 <br>
				 <strong class="contact">Fax</strong>: 852 2119 8089 <br>
				 <strong class="contact">Email</strong>: <a href="">info@eip.com.hk</a>
			  </p>
	   </div> 
	  </div>
	 </div>
   <!--end left sidebar-->